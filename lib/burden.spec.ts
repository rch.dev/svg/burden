import { burden } from './burden';
import { expect } from 'chai';
import 'mocha';

describe('Intent declaration function', () => {

  it('should stay something about svg', () => {
    const result = burden();
    expect(result).to.contain('svg');
  });

});